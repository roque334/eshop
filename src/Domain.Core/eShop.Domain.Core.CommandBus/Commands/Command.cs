using System;
using MediatR;

namespace eShop.Domain.Core.CommandBus.Commands
{
    public class Command : IRequest<bool>
    {
        public DateTime Timestamp { get; protected set; }
        protected Command()
        {
            Timestamp = DateTime.Now;
        }
    }
}