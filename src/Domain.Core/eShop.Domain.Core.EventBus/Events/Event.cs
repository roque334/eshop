using System;

namespace eShop.Domain.Core.EventBus.Events
{
    public class Event
    {
        public Guid Id { get; private set; }
        public DateTime CreationDate { get; protected set; }

        public Event()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }
    }
}