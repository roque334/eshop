using eShop.Domain.Core.EventBus.Events;

namespace eShop.Domain.Core.EventBus.Interfaces
{
    public interface IEventBus
    {
        void Publish<T>(T @event) where T : Event;
        void Subscribe<T, TH>()
           where T : Event
           where TH : IEventHandler<T>;

        void Unsubscribe<T, TH>()
            where T : Event
            where TH : IEventHandler<T>;
    }
}