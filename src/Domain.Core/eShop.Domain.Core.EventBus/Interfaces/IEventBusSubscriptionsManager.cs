using System;
using System.Collections.Generic;
using eShop.Domain.Core.EventBus.Events;

namespace eShop.Domain.Core.EventBus.Interfaces
{
    public interface IEventBusSubscriptionsManager
    {
        bool IsEmpty { get; }
        event EventHandler<string> OnEventRemoved;
        void AddSubscription<T, TH>()
            where T : Event
            where TH : IEventHandler<T>;

        void RemoveSubscription<T, TH>()
            where T : Event
            where TH : IEventHandler<T>;
        void Clear();

        string GetEventKey<T>();
        Type GetEventTypeByName(string eventName);
        bool HasSubscriptionsForEvent<T>() where T : Event;
        bool HasSubscriptionsForEvent(string eventName);
        IEnumerable<Type> GetHandlersForEvent<T>() where T : Event;
        IEnumerable<Type> GetHandlersForEvent(string eventName);
    }
}