using System.Threading.Tasks;
using eShop.Domain.Core.EventBus.Events;

namespace eShop.Domain.Core.EventBus.Interfaces
{
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        Task Handle(TEvent @event);
    }

    public interface IEventHandler
    {

    }
}