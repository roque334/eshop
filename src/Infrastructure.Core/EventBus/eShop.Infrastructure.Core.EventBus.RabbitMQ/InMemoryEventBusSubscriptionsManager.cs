using System;
using System.Collections.Generic;
using System.Linq;
using eShop.Domain.Core.EventBus.Events;
using eShop.Domain.Core.EventBus.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace eShop.Infrastructure.Core.EventBus.RabbitMQ
{
    public class InMemoryEventBusSubscriptionsManager : IEventBusSubscriptionsManager
    {
        private readonly Dictionary<string, List<Type>> _handlers;
        private readonly List<Type> _eventTypes;

        public event EventHandler<string> OnEventRemoved;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public InMemoryEventBusSubscriptionsManager(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _handlers = new Dictionary<string, List<Type>>();
            _eventTypes = new List<Type>();
        }

        public bool IsEmpty => throw new NotImplementedException();

        public void Clear()
        {
            _handlers.Clear();
            _eventTypes.Clear();
        }

        public void AddSubscription<T, TH>()
            where T : Event
            where TH : IEventHandler<T>
        {
            var eventType = typeof(T);
            var handlerType = typeof(TH);
            var eventName = eventType.Name;

            if (!_eventTypes.Contains(eventType))
                _eventTypes.Add(eventType);

            if (!_handlers.ContainsKey(eventName))
                _handlers.Add(eventName, new List<Type>());

            if (_handlers[eventName].Any(h => h.GetType() == handlerType))
                throw new InvalidOperationException($"Handler Type {handlerType.Name} already registered for '{eventName}'");

            _handlers[eventName].Add(handlerType);
        }

        public void RemoveSubscription<T, TH>()
            where T : Event
            where TH : IEventHandler<T>
        {
            var eventType = typeof(T);
            var handlerType = typeof(TH);
            var eventName = eventType.Name;


            if (_handlers.ContainsKey(eventName))
            {
                if (_handlers[eventName].Any(h => h.GetType() == handlerType))
                {
                    _handlers[eventName].Remove(handlerType);
                    if (!_handlers[eventName].Any())
                    {
                        _handlers.Remove(eventName);

                        if (!_eventTypes.Contains(eventType))
                        {
                            _eventTypes.Remove(eventType);
                        }
                        RaiseOnEventRemoved(eventName);
                    }
                }
            }
        }

        private void RaiseOnEventRemoved(string eventName)
        {
            var handler = OnEventRemoved;
            handler?.Invoke(this, eventName);
        }

        public string GetEventKey<T>()
        {
            return typeof(T).Name;
        }

        public Type GetEventTypeByName(string eventName)
        {
            return _eventTypes.SingleOrDefault(e => e.Name == eventName);
        }

        public bool HasSubscriptionsForEvent<T>() where T : Event
        {
            var eventName = GetEventKey<T>();
            return HasSubscriptionsForEvent(eventName);
        }

        public bool HasSubscriptionsForEvent(string eventName)
        {
            return _handlers.ContainsKey(eventName);
        }

        public IEnumerable<Type> GetHandlersForEvent<T>() where T : Event
        {
            var eventName = GetEventKey<T>();
            return GetHandlersForEvent(eventName);
        }

        public IEnumerable<Type> GetHandlersForEvent(string eventName)
        {
            return _handlers[eventName];
        }
    }
}