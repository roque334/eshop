using System;
using System.Net;
using System.Threading.Tasks;
using eShop.Basket.Application.Cqrs.Commands;
using eShop.Basket.Application.Cqrs.Queries;
using eShop.Basket.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace eShop.Basket.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BasketController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerBasket), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerBasket>> GetBasketByIdAsync(string id)
        {
            var detailsCustomerBasketQuery = new DetailsCustomerBasketQuery() { Id = id };
            return await _mediator.Send(detailsCustomerBasketQuery);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CustomerBasket), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CustomerBasket>> UpdateBasketAsync([FromBody] UpdateCustomerBasketCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        public async Task DeleteBasketByIdAsync(string id)
        {
            var deleteCustomerBasketCommand = new DeleteCustomerBasketCommand() { Id = id };
            await _mediator.Send(deleteCustomerBasketCommand);
        }

        [HttpPost]
        [Route("checkout")]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<ActionResult> CheckoutAsync([FromBody] BasketCheckoutCommand command, [FromHeader(Name = "x-reqestid")] string requestId)
        {
            command.RequestId =
                (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty)
                ? guid
                : command.RequestId;

            await _mediator.Send(command);

            return Accepted();
        }


    }
}