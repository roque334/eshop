using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using eShop.Basket.Application.Cqrs.Commands;
using eShop.Basket.Domain.Events;
using eShop.Basket.Domain.Exceptions;
using eShop.Basket.Domain.Interfaces;
using eShop.Basket.Domain.Models;
using eShop.Domain.Core.EventBus.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace eShop.Basket.Application.Cqrs.CommandHandlers
{
    public class BasketCheckoutCommandHandler : IRequestHandler<BasketCheckoutCommand>
    {
        private readonly IBasketRepository _repository;
        private readonly IIdentityAcessor _identityAccessor;
        private readonly IEventBus _eventBus;
        private readonly ILogger<BasketCheckoutCommandHandler> _logger;
        public BasketCheckoutCommandHandler(IBasketRepository repository, IIdentityAcessor identityAccessor, IEventBus eventBus, ILogger<BasketCheckoutCommandHandler> logger)
        {
            _repository = repository;
            _identityAccessor = identityAccessor;
            _eventBus = eventBus;
            _logger = logger;
        }

        public async Task<Unit> Handle(BasketCheckoutCommand request, CancellationToken cancellationToken)
        {
            var userId = _identityAccessor.GetUserIdentity();
            var userName = _identityAccessor.GetUserName();

            var basket = await _repository.GetBasketAsync(userId);

            if (basket == null)
                throw new BasketException(HttpStatusCode.BadRequest, new { basket = "Basket does not exists." });

            var eventMessage = new UserCheckoutAcceptedIntegrationEvent(userId, userName, request.City, request.Street,
                request.State, request.Country, request.ZipCode, request.CardNumber, request.CardHolderName,
                request.CardExpiration, request.CardSecurityNumber, request.CardTypeId, request.Buyer, request.RequestId, basket);

            // Once basket is checkout, sends an integration event to
            // ordering.api to convert basket to order and proceeds with
            // order creation process
            try
            {
                _eventBus.Publish(eventMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR Publishing integration event: {IntegrationEventId} from {AppName}", eventMessage.Id, typeof(BasketCheckoutCommandHandler).Assembly.GetName());

                throw;
            }

            return Unit.Value;
        }
    }
}