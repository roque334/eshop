using System.Threading;
using System.Threading.Tasks;
using eShop.Basket.Application.Cqrs.Commands;
using eShop.Basket.Domain.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;

namespace eShop.Basket.Application.Cqrs.CommandHandlers
{
    public class DeleteCustomerBasketCommandHandler : IRequestHandler<DeleteCustomerBasketCommand>
    {
        private readonly IBasketRepository _repository;
        private readonly ILogger<DeleteCustomerBasketCommandHandler> _logger;
        public DeleteCustomerBasketCommandHandler(IBasketRepository repository, ILogger<DeleteCustomerBasketCommandHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }
        public async Task<Unit> Handle(DeleteCustomerBasketCommand request, CancellationToken cancellationToken)
        {
            await _repository.DeleteBasketAsync(request.Id);
            return Unit.Value; ;
        }
    }
}