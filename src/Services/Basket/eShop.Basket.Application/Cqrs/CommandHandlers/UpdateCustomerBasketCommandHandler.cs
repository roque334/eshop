using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Basket.Application.Cqrs.Commands;
using eShop.Basket.Domain.Interfaces;
using eShop.Basket.Domain.Models;
using MediatR;

namespace eShop.Basket.Application.Cqrs.CommandHandlers
{
    public class UpdateCustomerBasketCommandHandler : IRequestHandler<UpdateCustomerBasketCommand, CustomerBasket>
    {
        private readonly IBasketRepository _repository;
        private readonly IMapper _mapper;

        public UpdateCustomerBasketCommandHandler(IBasketRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CustomerBasket> Handle(UpdateCustomerBasketCommand request, CancellationToken cancellationToken)
        {
            var customerBasket = _mapper.Map<UpdateCustomerBasketCommand, CustomerBasket>(request);
            return await _repository.UpdateBasketAsync(customerBasket);
        }
    }
}