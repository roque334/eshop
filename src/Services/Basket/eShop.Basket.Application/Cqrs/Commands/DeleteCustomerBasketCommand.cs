using MediatR;

namespace eShop.Basket.Application.Cqrs.Commands
{
    public class DeleteCustomerBasketCommand : IRequest
    {
        public string Id { get; set; }
    }
}