using System.Collections.Generic;
using eShop.Basket.Domain.Models;
using MediatR;

namespace eShop.Basket.Application.Cqrs.Commands
{
    public class UpdateCustomerBasketCommand : IRequest<CustomerBasket>
    {
        public string BuyerId { get; set; }

        public List<BasketItem> Items { get; set; }
    }
}