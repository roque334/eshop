using eShop.Basket.Domain.Models;
using MediatR;

namespace eShop.Basket.Application.Cqrs.Queries
{
    public class DetailsCustomerBasketQuery : IRequest<CustomerBasket>
    {
        public string Id { get; set; }
    }
}