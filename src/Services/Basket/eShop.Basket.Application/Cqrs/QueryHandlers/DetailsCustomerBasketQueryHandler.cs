using System.Threading;
using System.Threading.Tasks;
using eShop.Basket.Application.Cqrs.Queries;
using eShop.Basket.Domain.Interfaces;
using eShop.Basket.Domain.Models;
using MediatR;
using Microsoft.Extensions.Logging;

namespace eShop.Basket.Application.Cqrs.QueryHandlers
{
    public class DetailsCustomerBasketQueryHandler : IRequestHandler<DetailsCustomerBasketQuery, CustomerBasket>
    {
        private readonly IBasketRepository _repository;
        private readonly ILogger<DetailsCustomerBasketQueryHandler> _logger;
        public DetailsCustomerBasketQueryHandler(IBasketRepository repository, ILogger<DetailsCustomerBasketQueryHandler> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<CustomerBasket> Handle(DetailsCustomerBasketQuery request, CancellationToken cancellationToken)
        {
            var basket = await _repository.GetBasketAsync(request.Id);

            return basket ?? new CustomerBasket() { BuyerId = request.Id };
        }
    }
}