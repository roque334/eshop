using AutoMapper;
using eShop.Basket.Application.Cqrs.Commands;
using eShop.Basket.Domain.Models;

namespace eShop.Basket.Application.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UpdateCustomerBasketCommand, CustomerBasket>();
        }
    }
}