using System;
using System.Net;

namespace eShop.Basket.Domain.Exceptions
{
    public class BasketException : Exception
    {
        public HttpStatusCode Code { get; }
        public object Errors { get; }
        public BasketException(HttpStatusCode code, object errors = null)
        {
            this.Code = code;
            this.Errors = errors;
        }
    }
}