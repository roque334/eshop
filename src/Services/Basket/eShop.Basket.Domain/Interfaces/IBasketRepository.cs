using System.Collections.Generic;
using System.Threading.Tasks;
using eShop.Basket.Domain.Models;

namespace eShop.Basket.Domain.Interfaces
{
    public interface IBasketRepository
    {
        Task<CustomerBasket> GetBasketAsync(string customerId);
        IEnumerable<string> GetUsers();
        Task<CustomerBasket> UpdateBasketAsync(CustomerBasket basket);
        Task<bool> DeleteBasketAsync(string id);
    }
}