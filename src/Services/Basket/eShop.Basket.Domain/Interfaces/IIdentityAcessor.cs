namespace eShop.Basket.Domain.Interfaces
{
    public interface IIdentityAcessor
    {
        string GetUserIdentity();
        string GetUserName();
    }
}