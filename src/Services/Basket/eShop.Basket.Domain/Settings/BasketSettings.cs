namespace eShop.Basket.Domain.Settings
{
    public class BasketSettings
    {
        public string ConnectionString { get; set; }
    }
}