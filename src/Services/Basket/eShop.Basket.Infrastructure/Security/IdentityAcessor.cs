using System.Linq;
using System.Security.Claims;
using eShop.Basket.Domain.Interfaces;
using Microsoft.AspNetCore.Http;

namespace eShop.Basket.Infrastructure.Security
{
    public class IdentityAcessor : IIdentityAcessor
    {
        private readonly IHttpContextAccessor _context;
        public IdentityAcessor(IHttpContextAccessor context)
        {
            _context = context;
        }
        public string GetUserIdentity()
        {
            return _context.HttpContext.User.FindFirst("sub")?.Value;
        }

        public string GetUserName()
        {
            return _context.HttpContext.User.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
        }
    }
}