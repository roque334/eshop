using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using eShop.Catalog.Application.Models;
using eShop.Catalog.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using eShop.Catalog.Application.Cqrs.Queries;
using eShop.Catalog.Application.Cqrs.Commands;

namespace eShop.Catalog.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly IMediator _mediator;
        public CatalogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET api/v1/[controller]/items[?pageSize=10&pageIndex=0]
        [HttpGet]
        [Route("items")]
        [ProducesResponseType(typeof(PaginatedItems<CatalogItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<CatalogItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ItemsAsync([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 0, string ids = null)
        {
            var listQuery = new ListCatalogItems()
            {
                PageSize = pageSize,
                PageIndex = pageIndex,
                Ids = ids
            };

            var items = await _mediator.Send(listQuery);

            if (!string.IsNullOrEmpty(ids))
            {
                return Ok(items);
            }

            return Ok(new PaginatedItems<CatalogItem>(pageIndex, pageSize, items.LongCount(), items));
        }

        [HttpGet]
        [Route("items/{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(CatalogItem), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<CatalogItem>> ItemByIdAsync(int id)
        {
            return await _mediator.Send(new DetailsCatalogItem() { Id = id });
        }

        //POST api/v1/[controller]/items
        [HttpPost]
        [Route("items")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> CreateProductAsync([FromBody] CreateCatalogItem command)
        {
            var item = await _mediator.Send(command);

            return CreatedAtAction("items", new { id = item.Id }, null);
        }

        //PUT api/v1/[controller]/items
        [HttpPut]
        [Route("items")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> UpdateProductAsync([FromBody] UpdateCatalogItem command)
        {
            var item = await _mediator.Send(command);

            return CreatedAtAction("items", new { id = item.Id }, null);
        }

        //DELETE api/v1/[controller]/id
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<ActionResult> DeleteProductAsync(int id)
        {
            var item = await _mediator.Send(new DeleteCatalogItem() { Id = id });

            return NoContent();
        }

        // GET api/v1/[controller]/CatalogTypes
        [HttpGet]
        [Route("catalogtypes")]
        [ProducesResponseType(typeof(IEnumerable<CatalogType>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CatalogType>>> CatalogTypesAsync()
        {
            var catalogTypes = await _mediator.Send(new ListCatalogTypes());
            return Ok(catalogTypes);
        }

        // GET api/v1/[controller]/CatalogBrands
        [HttpGet]
        [Route("catalogbrands")]
        [ProducesResponseType(typeof(IEnumerable<CatalogBrand>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<CatalogBrand>>> CatalogBrandsAsync()
        {
            var catalogBrands = await _mediator.Send(new ListCatalogBrands());
            return Ok(catalogBrands);
        }
    }
}