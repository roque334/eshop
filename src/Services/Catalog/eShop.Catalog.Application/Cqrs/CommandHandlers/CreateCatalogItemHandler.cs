using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Catalog.Application.Cqrs.Commands;
using eShop.Catalog.Domain.Exceptions;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.CommandHandlers
{
    public class CreateCatalogItemHandler : IRequestHandler<CreateCatalogItem, CatalogItem>
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly IMapper _mapper;
        public CreateCatalogItemHandler(ICatalogRepository catalogRepository, IMapper mapper)
        {
            _mapper = mapper;
            _catalogRepository = catalogRepository;
        }
        public async Task<CatalogItem> Handle(CreateCatalogItem request, CancellationToken cancellationToken)
        {
            if ((await _catalogRepository.GetCatalogItemAsync(request.Id)) == null)
                throw new CatalogException(HttpStatusCode.BadRequest, new { catalogItem = "The item already exists." });

            var catalogItem = _mapper.Map<CreateCatalogItem, CatalogItem>(request);

            var item = await _catalogRepository.AddAsync(catalogItem);

            var nChanges = await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            if (nChanges > 0) return item;

            throw new System.Exception("Problem saving changes.");
        }
    }
}