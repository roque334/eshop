using System.Net;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Application.Cqrs.Commands;
using eShop.Catalog.Domain.Exceptions;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.CommandHandlers
{
    public class DeleteCatalogItemHandler : IRequestHandler<DeleteCatalogItem, CatalogItem>
    {
        private readonly ICatalogRepository _catalogRepository;
        public DeleteCatalogItemHandler(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }

        public async Task<CatalogItem> Handle(DeleteCatalogItem request, CancellationToken cancellationToken)
        {
            if ((await _catalogRepository.GetCatalogItemAsync(request.Id)) == null)
                throw new CatalogException(HttpStatusCode.NotFound, new { catalogItem = "Not Found" });

            var item = await _catalogRepository.RemoveAsync(request.Id);
            var nChanges = await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            if (nChanges > 0) return item;

            throw new System.Exception("Problem saving changes");
        }
    }
}