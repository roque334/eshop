using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Catalog.Application.Cqrs.Commands;
using eShop.Catalog.Domain.Exceptions;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.CommandHandlers
{
    public class UpdateCatalogItemHandler : IRequestHandler<UpdateCatalogItem, CatalogItem>
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly IMapper _mapper;
        public UpdateCatalogItemHandler(ICatalogRepository catalogRepository, IMapper mapper)
        {
            _mapper = mapper;
            _catalogRepository = catalogRepository;
        }
        public async Task<CatalogItem> Handle(UpdateCatalogItem request, CancellationToken cancellationToken)
        {
            var catalogItem = await _catalogRepository.GetCatalogItemAsync(request.Id);
            if (catalogItem == null)
                throw new CatalogException(HttpStatusCode.NotFound, new { catalogItem = "Not Found" });

            _mapper.Map<UpdateCatalogItem, CatalogItem>(request, catalogItem);

            await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            return catalogItem;
        }
    }
}