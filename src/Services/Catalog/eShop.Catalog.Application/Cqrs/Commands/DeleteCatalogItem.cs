using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.Commands
{
    public class DeleteCatalogItem : IRequest<CatalogItem>
    {
        public int Id { get; set; }
    }
}