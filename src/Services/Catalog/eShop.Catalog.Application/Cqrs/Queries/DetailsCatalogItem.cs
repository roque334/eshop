using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.Queries
{
    public class DetailsCatalogItem : IRequest<CatalogItem>
    {
        public int Id { get; set; }
    }
}