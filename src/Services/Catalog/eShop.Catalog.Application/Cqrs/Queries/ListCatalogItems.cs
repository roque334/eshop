using System.Collections.Generic;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.Queries
{
    public class ListCatalogItems : IRequest<IEnumerable<CatalogItem>>
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Ids { get; set; }
    }
}