using System.Collections.Generic;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.Queries
{
    public class ListCatalogTypes : IRequest<IEnumerable<CatalogType>>
    {

    }
}