using System.Net;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Exceptions;
using eShop.Catalog.Application.Cqrs.Queries;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.QueryHandlers
{
    public class DetailsCatalogItemHandler : IRequestHandler<DetailsCatalogItem, CatalogItem>
    {
        private readonly ICatalogRepository _catalogRepository;
        public DetailsCatalogItemHandler(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }

        public async Task<CatalogItem> Handle(DetailsCatalogItem request, CancellationToken cancellationToken)
        {
            var catalogItem = await _catalogRepository.GetCatalogItemAsync(request.Id);

            if (catalogItem == null)
                throw new CatalogException(HttpStatusCode.NotFound, new { catalogItem = "Not Found" });

            return catalogItem;
        }
    }
}