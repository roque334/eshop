using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Application.Cqrs.Queries;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.QueryHandlers
{
    public class ListCatalogBrandsHandler : IRequestHandler<ListCatalogBrands, IEnumerable<CatalogBrand>>
    {
        private readonly ICatalogRepository _catalogRepository;
        public ListCatalogBrandsHandler(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }
        public Task<IEnumerable<CatalogBrand>> Handle(ListCatalogBrands request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_catalogRepository.GetCatalogBrands());
        }
    }
}