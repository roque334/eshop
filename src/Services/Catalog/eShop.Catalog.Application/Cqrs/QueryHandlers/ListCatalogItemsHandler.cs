using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Application.Cqrs.Queries;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.QueryHandlers
{
    public class ListCatalogItemsHandler : IRequestHandler<ListCatalogItems, IEnumerable<CatalogItem>>
    {
        private readonly ICatalogRepository _catalogRepository;
        public ListCatalogItemsHandler(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }
        public Task<IEnumerable<CatalogItem>> Handle(ListCatalogItems request, CancellationToken cancellationToken)
        {
            IEnumerable<CatalogItem> items = null;

            if (!string.IsNullOrEmpty(request.Ids))
            {
                var numIds = request.Ids.Split(',').Select(id => (Ok: int.TryParse(id, out int x), Value: x));
                if (!numIds.All(nid => nid.Ok))
                {
                    return Task.FromResult(default(IEnumerable<CatalogItem>));
                }

                var idsToSelect = numIds
                    .Select(id => id.Value);

                items = _catalogRepository.GetCatalogItems()
                    .Where(ci => idsToSelect.Contains(ci.Id))
                    .OrderBy(c => c.Name);
            }
            else
            {
                var catalogItems = _catalogRepository.GetCatalogItems();
                var totalItems = catalogItems.LongCount();
                items = catalogItems
                    .OrderBy(c => c.Name)
                    .Skip(request.PageSize * request.PageIndex)
                    .Take(request.PageSize);
            }

            if (!items.Any())
                throw new Exception("ids value invalid. Must be comma-separated list of numbers");

            return Task.FromResult(items);
        }
    }
}