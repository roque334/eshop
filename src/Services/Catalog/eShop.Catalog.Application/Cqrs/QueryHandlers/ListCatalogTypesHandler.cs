using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Application.Cqrs.Queries;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using MediatR;

namespace eShop.Catalog.Application.Cqrs.QueryHandlers
{
    public class ListCatalogTypesHandler : IRequestHandler<ListCatalogTypes, IEnumerable<CatalogType>>
    {
        private readonly ICatalogRepository _catalogRepository;
        public ListCatalogTypesHandler(ICatalogRepository catalogRepository)
        {
            _catalogRepository = catalogRepository;
        }
        public Task<IEnumerable<CatalogType>> Handle(ListCatalogTypes request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_catalogRepository.GetCatalogTypes());
        }
    }
}