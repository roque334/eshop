using eShop.Catalog.Application.Cqrs.Queries;
using FluentValidation;

namespace eShop.Catalog.Application.Cqrs.Validators
{
    public class DeleteCatalogItemValidator : AbstractValidator<DetailsCatalogItem>
    {
        public DeleteCatalogItemValidator()
        {
            RuleFor(x => x.Id).NotEmpty().GreaterThan(0);
        }
    }
}