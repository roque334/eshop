using eShop.Catalog.Application.Cqrs.Queries;
using FluentValidation;

namespace eShop.Catalog.Application.Cqrs.Validators
{
    public class DetailsCatalogItemValidator : AbstractValidator<DetailsCatalogItem>
    {
        public DetailsCatalogItemValidator()
        {
            RuleFor(x => x.Id).NotEmpty().GreaterThan(0);
        }
    }
}