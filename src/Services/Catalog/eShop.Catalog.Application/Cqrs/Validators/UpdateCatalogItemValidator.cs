using eShop.Catalog.Application.Cqrs.Commands;
using FluentValidation;

namespace eShop.Catalog.Application.Cqrs.Validators
{
    public class UpdateCatalogItemValidator : AbstractValidator<UpdateCatalogItem>
    {
        public UpdateCatalogItemValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
            RuleFor(x => x.Name).NotEmpty().MaximumLength(50);
            RuleFor(x => x.Price).NotEmpty();
            RuleFor(x => x.PictureFileName).NotEmpty();
            RuleFor(x => x.CatalogTypeId).NotEmpty();
            RuleFor(x => x.CatalogBrandId).NotEmpty();
        }
    }
}