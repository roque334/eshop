using System;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Interfaces;
using eShop.Domain.Core.EventBus.Events;
using eShop.Domain.Core.EventBus.Interfaces;
using Microsoft.Extensions.Logging;

namespace eShop.Catalog.Application.EventServices
{
    public class CatalogIntegrationEventService : ICatalogIntegrationEventService, IDisposable
    {
        private readonly ILogger<CatalogIntegrationEventService> _logger;
        private readonly IEventBus _eventBus;
        private readonly ICatalogRepository _catalogRepository;
        private volatile bool disposedValue;

        public CatalogIntegrationEventService(
            ILogger<CatalogIntegrationEventService> logger,
            IEventBus eventBus,
            ICatalogRepository catalogRepository)
        {
            _logger = logger;
            _eventBus = eventBus;
            _catalogRepository = catalogRepository;
        }

        public Task PublishThroughEventBusAsync(Event evt)
        {
            try
            {
                _logger.LogInformation($"----- Publishing event: {evt.Id} from {this.GetType().Assembly.GetName()} - {evt}");
                _eventBus.Publish(evt);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"ERROR Publishing event: {evt.Id} from {this.GetType().Assembly.GetName()} - {evt}");
            }

            return Task.CompletedTask;
        }

        public async Task SaveEventAndCatalogContextChangesAsync(Event evt)
        {
            try
            {
                _logger.LogInformation($"----- CatalogIntegrationEventService - Saving changes and event: {evt.Id}");
                await _catalogRepository.UnitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex, $"ERROR Saving changes and event: {evt.Id}");
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //(_eventLogService as IDisposable)?.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}