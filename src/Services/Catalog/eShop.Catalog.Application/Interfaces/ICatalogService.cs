using System.Collections.Generic;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Models;

namespace eShop.Catalog.Application.Interfaces
{
    public interface ICatalogService
    {
        Task<List<CatalogItem>> GetItemsByIdsAsync(int pageSize, int pageIndex, string ids);

        Task<CatalogItem> GetItemByIdAsync(int id);
        Task<CatalogItem> CreateItemAsync(CatalogItem catalogItem);
        Task<CatalogItem> UpdateItemAsync(CatalogItem catalogItem);
        Task<CatalogItem> DeleteItemAsync(int id);
        Task<List<CatalogType>> GetCatalogTypesAsync();
        Task<List<CatalogBrand>> GetCatalogBrandsAsync();
    }
}