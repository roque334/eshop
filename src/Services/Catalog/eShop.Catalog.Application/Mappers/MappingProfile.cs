using AutoMapper;
using eShop.Catalog.Application.Cqrs.Commands;
using eShop.Catalog.Domain.Models;

namespace eShop.Catalog.Application.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CreateCatalogItem, CatalogItem>();
            CreateMap<UpdateCatalogItem, CatalogItem>();
        }
    }
}