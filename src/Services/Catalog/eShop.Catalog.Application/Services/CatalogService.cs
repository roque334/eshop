using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Application.Interfaces;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using eShop.Catalog.Domain.Settings;
using Microsoft.Extensions.Options;

namespace eShop.Catalog.Application.Services
{
    public class CatalogService : ICatalogService
    {

        private readonly ICatalogRepository _catalogRepository;
        private readonly CatalogSettings _settings;
        private readonly ICatalogIntegrationEventService _catalogIntegrationEventService;
        public CatalogService(ICatalogRepository catalogRepository, IOptionsSnapshot<CatalogSettings> settings, ICatalogIntegrationEventService catalogIntegrationEventService)
        {
            _catalogRepository = catalogRepository;
            _catalogIntegrationEventService = catalogIntegrationEventService ?? throw new ArgumentNullException(nameof(catalogIntegrationEventService)); ;
            _settings = settings.Value;
        }

        public async Task<CatalogItem> GetItemByIdAsync(int id)
        {
            var item = await _catalogRepository.GetCatalogItemAsync(id);

            // var baseUri = _settings.PicBaseUrl;
            // var azureStorageEnabled = _settings.AzureStorageEnabled;
            // item.FillProductUrl(baseUri, azureStorageEnabled: azureStorageEnabled);

            return item;
        }

        public Task<List<CatalogItem>> GetItemsByIdsAsync(int pageSize = 10, int pageIndex = 0, string ids = null)
        {
            List<CatalogItem> result;

            if (!string.IsNullOrEmpty(ids))
            {
                var numIds = ids.Split(',').Select(id => (Ok: int.TryParse(id, out int x), Value: x));

                if (!numIds.All(nid => nid.Ok))
                {
                    return Task.FromResult(new List<CatalogItem>());
                }

                var idsToSelect = numIds
                    .Select(id => id.Value);

                result = _catalogRepository.GetCatalogItems().Where(ci => idsToSelect.Contains(ci.Id)).ToList();
            }
            else
            {
                var catalogItems = _catalogRepository.GetCatalogItems();
                var totalItems = catalogItems.LongCount();
                result = catalogItems
                    .OrderBy(c => c.Name)
                    .Skip(pageSize * pageIndex)
                    .Take(pageSize)
                    .ToList();
            }

            //result = ChangeUriPlaceholder(items);

            return Task.FromResult(result);
        }

        public async Task<CatalogItem> CreateItemAsync(CatalogItem catalogItem)
        {
            var item = await _catalogRepository.AddAsync(catalogItem);

            if (item != null)
                await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            return item;
        }

        public async Task<CatalogItem> UpdateItemAsync(CatalogItem catalogItem)
        {
            var item = await _catalogRepository.UpdateAsync(catalogItem);

            if (item != null)
                await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            return item;
        }

        public async Task<CatalogItem> DeleteItemAsync(int id)
        {
            var item = await _catalogRepository.RemoveAsync(id);

            if (item != null)
                await _catalogRepository.UnitOfWork.SaveChangesAsync(CancellationToken.None);

            return item;
        }

        public async Task<List<CatalogType>> GetCatalogTypesAsync()
        {
            return await _catalogRepository.GetCatalogTypesAsync();
        }

        public async Task<List<CatalogBrand>> GetCatalogBrandsAsync()
        {
            return await _catalogRepository.GetCatalogBrandsAsync();
        }
    }
}