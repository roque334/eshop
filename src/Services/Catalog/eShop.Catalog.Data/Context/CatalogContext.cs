using System.Threading;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace eShop.Catalog.Data.Context
{
    public class CatalogContext : DbContext, IUnitOfWork
    {
        public CatalogContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<CatalogItem> CatalogItems { get; set; }
        public DbSet<CatalogBrand> CatalogBrands { get; set; }
        public DbSet<CatalogType> CatalogTypes { get; set; }
    }
}