using System.Collections.Generic;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Events;
using eShop.Catalog.Domain.Interfaces;
using eShop.Domain.Core.EventBus.Interfaces;
using Microsoft.Extensions.Logging;
using System.Linq;
using eShop.Domain.Core.EventBus.Events;

namespace eShop.Catalog.Domain.EventHandlers
{
    public class OrderStatusChangedToAwaitingValidationIntegrationEventHandler : IEventHandler<OrderStatusChangedToAwaitingValidationIntegrationEvent>
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly ICatalogIntegrationEventService _catalogIntegrationEventService;
        private readonly ILogger<OrderStatusChangedToAwaitingValidationIntegrationEventHandler> _logger;

        public OrderStatusChangedToAwaitingValidationIntegrationEventHandler(
            ICatalogRepository catalogRepository,
            ICatalogIntegrationEventService catalogIntegrationEventService,
            ILogger<OrderStatusChangedToAwaitingValidationIntegrationEventHandler> logger)
        {
            _catalogRepository = catalogRepository;
            _catalogIntegrationEventService = catalogIntegrationEventService;
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        public async Task Handle(OrderStatusChangedToAwaitingValidationIntegrationEvent @event)
        {
            _logger.LogInformation($"----- Handling integration event: {@event.Id} at {this.GetType().Assembly.GetName()} - ({@event})");
            var confirmedOrderStockItems = new List<ConfirmedOrderStockItem>();

            foreach (var orderStockItem in @event.OrderStockItems)
            {
                var catalogItem = _catalogRepository.GetCatalogItems().SingleOrDefault(ci => ci.Id == orderStockItem.ProductId);
                var hasStock = catalogItem.AvailableStock >= orderStockItem.Units;
                var confirmedOrderStockItem = new ConfirmedOrderStockItem(catalogItem.Id, hasStock);

                confirmedOrderStockItems.Add(confirmedOrderStockItem);
            }

            var confirmedIntegrationEvent = confirmedOrderStockItems.Any(i => !i.HasStock)
                ? (Event)new OrderStockRejectedIntegrationEvent(@event.OrderId, confirmedOrderStockItems)
                : (Event)new OrderStockConfirmedIntegrationEvent(@event.OrderId);

            await _catalogIntegrationEventService.SaveEventAndCatalogContextChangesAsync(confirmedIntegrationEvent);
            await _catalogIntegrationEventService.PublishThroughEventBusAsync(confirmedIntegrationEvent);
        }
    }
}