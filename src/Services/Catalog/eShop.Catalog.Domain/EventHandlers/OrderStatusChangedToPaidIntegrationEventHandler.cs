using System.Linq;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Events;
using eShop.Catalog.Domain.Interfaces;
using eShop.Domain.Core.EventBus.Interfaces;
using Microsoft.Extensions.Logging;

namespace eShop.Catalog.Domain.EventHandlers
{
    public class OrderStatusChangedToPaidIntegrationEventHandler : IEventHandler<OrderStatusChangedToPaidIntegrationEvent>
    {
        private readonly ICatalogRepository _catalogRepository;
        private readonly ILogger<OrderStatusChangedToPaidIntegrationEventHandler> _logger;

        public OrderStatusChangedToPaidIntegrationEventHandler(
            ICatalogRepository catalogRepository,
            ILogger<OrderStatusChangedToPaidIntegrationEventHandler> logger)
        {
            _catalogRepository = catalogRepository;
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        public async Task Handle(OrderStatusChangedToPaidIntegrationEvent @event)
        {
            _logger.LogInformation($"----- Handling integration event: {@event.Id} at {this.GetType().Assembly.GetName()} - ({@event})");

            foreach (var orderStockItem in @event.OrderStockItems)
            {
                var catalogItem = _catalogRepository.GetCatalogItems().SingleOrDefault(ci => ci.Id == orderStockItem.ProductId);

                catalogItem?.RemoveStock(orderStockItem.Units);
            }

            await _catalogRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}