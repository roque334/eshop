using System.Collections.Generic;
using eShop.Domain.Core.EventBus.Events;

namespace eShop.Catalog.Domain.Events
{
    public class OrderStatusChangedToPaidIntegrationEvent : Event
    {
        public int OrderId { get; }
        public IEnumerable<OrderStockItem> OrderStockItems { get; }

        public OrderStatusChangedToPaidIntegrationEvent(int orderId, IEnumerable<OrderStockItem> orderStockItems)
        {
            OrderId = orderId;
            OrderStockItems = orderStockItems;
        }
    }
}