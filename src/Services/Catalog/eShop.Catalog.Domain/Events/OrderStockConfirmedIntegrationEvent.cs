using eShop.Domain.Core.EventBus.Events;

namespace eShop.Catalog.Domain.Events
{
    public class OrderStockConfirmedIntegrationEvent : Event
    {
        public int OrderId { get; }

        public OrderStockConfirmedIntegrationEvent(int orderId)
        {
            OrderId = orderId;
        }
    }
}