using eShop.Domain.Core.EventBus.Events;

namespace eShop.Catalog.Domain.Events
{
    public class ProductPriceChangedIntegrationEvent : Event
    {
        public int ProductId { get; private set; }

        public decimal NewPrice { get; private set; }

        public decimal OldPrice { get; private set; }

        public ProductPriceChangedIntegrationEvent(int productId, decimal newPrice, decimal oldPrice)
        {
            ProductId = productId;
            NewPrice = newPrice;
            OldPrice = oldPrice;
        }
    }
}