using System;
using System.Net;

namespace eShop.Catalog.Domain.Exceptions
{
    public class CatalogException : Exception
    {
        public HttpStatusCode Code { get; }
        public object Errors { get; }
        public CatalogException(HttpStatusCode code, object errors = null)
        {
            this.Code = code;
            this.Errors = errors;
        }
    }
}