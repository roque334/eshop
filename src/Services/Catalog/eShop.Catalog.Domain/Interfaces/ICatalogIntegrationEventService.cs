using System.Threading.Tasks;
using eShop.Domain.Core.EventBus.Events;

namespace eShop.Catalog.Domain.Interfaces
{
    public interface ICatalogIntegrationEventService
    {
        Task SaveEventAndCatalogContextChangesAsync(Event evt);
        Task PublishThroughEventBusAsync(Event evt);
    }
}