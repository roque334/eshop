using System.Collections.Generic;
using System.Threading.Tasks;
using eShop.Catalog.Domain.Models;

namespace eShop.Catalog.Domain.Interfaces
{
    public interface ICatalogRepository : IRepository<CatalogItem>
    {
        IEnumerable<CatalogItem> GetCatalogItems();

        Task<List<CatalogItem>> GetCatalogItemsAsync();

        Task<CatalogItem> GetCatalogItemAsync(int catalogId);

        Task<CatalogItem> AddAsync(CatalogItem catalogItem);

        Task<CatalogItem> UpdateAsync(CatalogItem catalogItem);

        Task<CatalogItem> RemoveAsync(int catalogId);

        IEnumerable<CatalogType> GetCatalogTypes();

        Task<List<CatalogType>> GetCatalogTypesAsync();

        IEnumerable<CatalogBrand> GetCatalogBrands();

        Task<List<CatalogBrand>> GetCatalogBrandsAsync();
    }
}