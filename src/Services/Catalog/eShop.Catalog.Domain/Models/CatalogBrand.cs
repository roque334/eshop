namespace eShop.Catalog.Domain.Models
{
    public class CatalogBrand
    {
        public int Id { get; set; }
        public string Brand { get; set; }
    }
}