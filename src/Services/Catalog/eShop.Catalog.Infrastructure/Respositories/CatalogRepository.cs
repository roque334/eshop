using System.Collections.Generic;
using System.Threading.Tasks;
using eShop.Catalog.Data.Context;
using eShop.Catalog.Domain.Interfaces;
using eShop.Catalog.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace eShop.Catalog.Infrastructure.Respositories
{
    public class CatalogRepository : ICatalogRepository
    {
        private readonly CatalogContext _context;

        public IUnitOfWork UnitOfWork { get { return _context; } }

        public CatalogRepository(CatalogContext context)
        {
            this._context = context;
        }

        public IEnumerable<CatalogItem> GetCatalogItems()
        {
            return _context.CatalogItems;
        }

        public async Task<List<CatalogItem>> GetCatalogItemsAsync()
        {
            return await _context.CatalogItems.ToListAsync();
        }

        public async Task<CatalogItem> GetCatalogItemAsync(int catalogId)
        {
            return await _context.CatalogItems.FindAsync(catalogId);
        }

        public async Task<CatalogItem> AddAsync(CatalogItem catalogItem)
        {
            await _context.AddAsync(catalogItem);
            return catalogItem;
        }

        public async Task<CatalogItem> UpdateAsync(CatalogItem catalogItem)
        {
            var itemToUpdate = await _context.CatalogItems.FindAsync(catalogItem.Id);

            if (itemToUpdate != null)
            {
                itemToUpdate = catalogItem;
                _context.CatalogItems.Update(itemToUpdate);
            }

            return itemToUpdate;
        }

        public async Task<CatalogItem> RemoveAsync(int catalogId)
        {
            var catalogItem = await _context.CatalogItems.FindAsync(catalogId);

            if (catalogItem != null)
                _context.CatalogItems.Remove(catalogItem);

            return catalogItem;
        }

        public async Task<List<CatalogType>> GetCatalogTypesAsync()
        {
            return await _context.CatalogTypes.ToListAsync();
        }

        public async Task<List<CatalogBrand>> GetCatalogBrandsAsync()
        {
            return await _context.CatalogBrands.ToListAsync();
        }

        public IEnumerable<CatalogType> GetCatalogTypes()
        {
            return _context.CatalogTypes;
        }

        public IEnumerable<CatalogBrand> GetCatalogBrands()
        {
            return _context.CatalogBrands;
        }
    }
}