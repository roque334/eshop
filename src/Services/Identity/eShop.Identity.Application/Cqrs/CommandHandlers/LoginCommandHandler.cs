using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Identity.Application.Cqrs.Commands;
using eShop.Identity.Application.Exceptions;
using eShop.Identity.Application.Interfaces;
using eShop.Identity.Application.Models;
using eShop.Identity.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace eShop.Identity.Application.Cqrs.CommandHandlers
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, User>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IJwtGenerator _jwtGenerator;
        public LoginCommandHandler(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IJwtGenerator jwtGenerator, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtGenerator = jwtGenerator;
            _mapper = mapper;
        }

        public async Task<User> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var unauthorizedMessage = "The email and password provided are incorrect";
            var applicationUser = await _userManager.FindByEmailAsync(request.Email);

            if (applicationUser == null) throw new AccountException(HttpStatusCode.Unauthorized, new { message = unauthorizedMessage });

            var signInResult = await _signInManager.CheckPasswordSignInAsync(applicationUser, request.Password, false);

            if (signInResult.Succeeded)
            {
                var result = _mapper.Map<ApplicationUser, User>(applicationUser);
                result.Token = _jwtGenerator.CreateToken(applicationUser);
                return result;
            }

            throw new AccountException(HttpStatusCode.Unauthorized, new { message = unauthorizedMessage });
        }
    }
}