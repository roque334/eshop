using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Identity.Application.Cqrs.Commands;
using eShop.Identity.Application.Exceptions;
using eShop.Identity.Application.Interfaces;
using eShop.Identity.Application.Models;
using eShop.Identity.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace eShop.Identity.Application.Cqrs.CommandHandlers
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, User>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IJwtGenerator _jwtGenerator;
        public RegisterCommandHandler(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IJwtGenerator jwtGenerator, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtGenerator = jwtGenerator;
            _mapper = mapper;
        }

        public async Task<User> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            if ((await _userManager.FindByEmailAsync(request.Email)) != null)
                throw new AccountException(HttpStatusCode.BadRequest, new { Email = "Email already exists" });

            if ((await _userManager.FindByNameAsync(request.UserName)) != null)
                throw new AccountException(HttpStatusCode.BadRequest, new { Username = "Username already exists" });

            var applicationUser = _mapper.Map<RegisterCommand, ApplicationUser>(request);

            var identityResult = await _userManager.CreateAsync(applicationUser, request.Password);

            if (identityResult.Succeeded)
            {
                var result = _mapper.Map<ApplicationUser, User>(applicationUser);
                result.Token = _jwtGenerator.CreateToken(applicationUser);
                return result;
            }

            throw new Exception("Problem creating user");
        }
    }
}