using eShop.Identity.Application.Models;
using MediatR;

namespace eShop.Identity.Application.Cqrs.Commands
{
    public class LoginCommand : IRequest<User>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}