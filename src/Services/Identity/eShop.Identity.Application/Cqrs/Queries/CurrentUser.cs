using eShop.Identity.Application.Models;
using MediatR;

namespace eShop.Identity.Application.Cqrs.Queries
{
    public class CurrentUserQuery : IRequest<User>
    {

    }
}