using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using eShop.Identity.Application.Cqrs.Queries;
using eShop.Identity.Application.Interfaces;
using eShop.Identity.Application.Models;
using eShop.Identity.Domain.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace eShop.Identity.Application.Cqrs.QueryHandlers
{
    public class CurrentUserQueryHandler : IRequestHandler<CurrentUserQuery, User>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserAccessor _userAccessor;
        private readonly IJwtGenerator _jwtGenerator;
        private readonly IMapper _mapper;

        public CurrentUserQueryHandler(UserManager<ApplicationUser> userManager, IUserAccessor userAccessor, IJwtGenerator jwtGenerator, IMapper mapper)
        {
            _mapper = mapper;
            _jwtGenerator = jwtGenerator;
            _userAccessor = userAccessor;
            _userManager = userManager;
        }

        public async Task<User> Handle(CurrentUserQuery request, CancellationToken cancellationToken)
        {
            var username = _userAccessor.GetCurrentUsername();
            var applicationUser = await _userManager.FindByNameAsync(username);

            if (applicationUser == null) return default(User);

            return _mapper.Map<ApplicationUser, User>(applicationUser);
        }
    }
}