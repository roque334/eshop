using eShop.Identity.Application.Cqrs.Commands;
using FluentValidation;

namespace eShop.Identity.Application.Cqrs.Validators
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}