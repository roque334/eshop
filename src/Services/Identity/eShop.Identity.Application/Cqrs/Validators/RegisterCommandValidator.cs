using eShop.Identity.Application.Cqrs.Commands;
using FluentValidation;

namespace eShop.Identity.Application.Cqrs.Validators
{
    public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
    {
        public RegisterCommandValidator()
        {
            RuleFor(x => x.CardNumber).NotEmpty();
            RuleFor(x => x.SecurityNumber).NotEmpty();
            RuleFor(x => x.Expiration).NotEmpty();
            RuleFor(x => x.Expiration).Matches(@"^(0[1-9]|1[0-2])\/[0-9]{2}$").WithMessage("Expiration should match a valid MM/YY value");
            RuleFor(x => x.CardHolderName).NotEmpty();
            RuleFor(x => x.CardType).NotEmpty();
            RuleFor(x => x.Street).NotEmpty();
            RuleFor(x => x.City).NotEmpty();
            RuleFor(x => x.State).NotEmpty();
            RuleFor(x => x.Country).NotEmpty();
            RuleFor(x => x.ZipCode).NotEmpty();
            RuleFor(x => x.FirtName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.Password).Password();
        }
    }
}