using System;
using System.Net;

namespace eShop.Identity.Application.Exceptions
{
    public class AccountException : Exception
    {
        public HttpStatusCode Code { get; }
        public object Errors { get; }
        public AccountException(HttpStatusCode code, object errors = null)
        {
            this.Code = code;
            this.Errors = errors;
        }
    }
}