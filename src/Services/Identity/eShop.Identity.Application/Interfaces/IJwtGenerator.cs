using eShop.Identity.Domain.Models;

namespace eShop.Identity.Application.Interfaces
{
    public interface IJwtGenerator
    {
        string CreateToken(ApplicationUser user);
    }
}