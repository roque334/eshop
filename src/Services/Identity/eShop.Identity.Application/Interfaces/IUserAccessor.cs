namespace eShop.Identity.Application.Interfaces
{
    public interface IUserAccessor
    {
        string GetCurrentUsername();
    }
}