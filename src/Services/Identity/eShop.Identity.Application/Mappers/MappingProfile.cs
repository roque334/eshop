using AutoMapper;
using eShop.Identity.Application.Cqrs.Commands;
using eShop.Identity.Application.Models;
using eShop.Identity.Domain.Models;

namespace eShop.Identity.Application.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<RegisterCommand, ApplicationUser>();
            CreateMap<ApplicationUser, User>();
        }
    }
}