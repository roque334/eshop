using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using eShop.Identity.Application.Exceptions;
using eShop.Identity.Infrastructure.ActionResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace eShop.Identity.Infrastructure.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;

        public HttpGlobalExceptionFilter(IWebHostEnvironment env, ILogger<HttpGlobalExceptionFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            switch (context.Exception)
            {
                case AccountException ae:
                    context.Result = new BadRequestObjectResult(new { Errors = ae.Errors });
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    break;
                case Exception e:
                    var developmentMessage = string.IsNullOrWhiteSpace(e.Message) ? "An error ocurred." : e.Message;
                    var message = _env.IsDevelopment() ? developmentMessage : "An error ocurred.";

                    context.Result = new InternalServerErrorObjectResult(new { Errors = message });
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }
            context.ExceptionHandled = true;
        }
    }
}